# Tire-Quality-Detection-Deep-Learning-Web-App

[Kaggle Dataset](https://www.kaggle.com/datasets/warcoder/tyre-quality-classification/data)

Referenced from Mr. MAINAK CHAUDHURI.
The detection is performed with the help of ResNet-50 and MobileNet V3 and is faster and lighter than its competitors.

Will update in weekends
